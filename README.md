### What is this repository for? ###

This ansible package can be used for the installation of both the Eagle Web and reporting server.  By default, it uses the "ansible" login via HTTPS WINRM.  Both of which can be configured in the group_vars.

### How do I get set up? ###

By default, there is a restriction on Powershell.  The following script needs to be run upon the completion of the provisioning of the Windows VM.  Along with permission, it also creates a self signed certificate and HTTPS listener.

https://eaglefiles.blob.core.windows.net/windows/ConfigureRemotingForAnsible.ps1


### Contribution guidelines ###

A total of 4 roles were created.

	-win_report_install
	-win_report_patch
	-win_web_install
	-win_web_patch

Both the "install" roles fulfill the pre-reqs for the installation.  Powershell commands are used to install Windows features and roles such as webserver and ASP.

[Web server]
---


 - name: Host configuration
   hosts: windows2012r2 
   roles:
      - win_web_install
      - win_web_patch

[Reporting server]
---


 - name: Host configuration
   hosts: windows2012r23
   roles:
      - win_report_install
      - win_report_patch
	  
[Ansible command line]
---
ansible-playbook windows2012r23_rep.yml


### Sequence ###

-create eagleadmin (administrator) account
-Build hosts file
-Install Windows Feature (web server)
-Configure IIS
-Disable Firewall
-Create data disk partition/format
-Download installation and pre-req files
-Install libraries and Eagle
-Reboot

Note: NxG silent installation is inconsistent. An entry will be added to the RunOnce registry node to kick silent installation upon logon with eagleadmin.

